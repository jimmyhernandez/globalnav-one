"use strict";

/**
 * TwitterQuote Sightly component JS backing script
 */
use(function () {

    var CONST = {
        PROP_URL: "url",
        PROP_QUOTE: "quote"
    }

    var url = granite.resource.properties[CONST.PROP_URL] || "";
    var quote = granite.resource.properties[CONST.PROP_QUOTE] || "";
    var parts = url.split("/");
    var id = (parts.length>1 ? parts[(parts.length - 1)] : url);

    return {
        url:url,
        quote:quote,
        id:id
    };

});
