var PANEL_CLASSIC_LISTENER = {

        backgroundTypeValidation : function(dialog) {
			var marqueeBGColor = dialog.getField('./marquebgcolor');
            var bgDirection = dialog.getField('./bggdirection');
            var bgColor1 = dialog.getField('./bggcolor1');
            var bgColor2 = dialog.getField('./bggcolor2');
            
            var backgroundTypeField = dialog.getField('./bgType');
            var backgroundType = backgroundTypeField.getRawValue();
            
            marqueeBGColor.hide();
            bgDirection.hide();
            bgColor1.hide();
            bgColor2.hide();
        
            var layout = dialog.getField('./layout').getValue();
            var sublayout = dialog.getField('./subLayout').getValue();
            
            if(layout == 'template-content-configurable' && (sublayout == 'bgc' || sublayout == 'bgcfgi' )) {
                if(backgroundType == 'Background Color') {
                    marqueeBGColor.show();		
                } else {		
                    bgDirection.show();
                    bgColor1.show();
                    bgColor2.show();
                }
            }

        },

};





