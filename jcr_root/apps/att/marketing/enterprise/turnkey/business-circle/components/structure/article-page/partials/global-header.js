'use strict';
var global = this;
use(function () {
    var resourceResolver = resource.getResourceResolver();
    var LOOKUP_PATH = '/jcr:content/parsys';
    var globalConfigService = undefined;
    var mobileLoginImagePath, assetsExternalizerService, externalizedUrl, authLoginService, loginLabel;
    if (global.sling && global.Packages) {
        globalConfigService = global.sling
            .getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
        assetsExternalizerService = 
            global.sling.getService(global.Packages.com.att.global.all.cms.common.core.services.AssetsExternalizer);                
        authLoginService = 
            global.sling.getService(global.Packages.com.att.global.all.cms.common.authentication.services.config.AuthConfig);

		var generalConfigPagePath = globalConfigService.getGeneralConfigPagePath();

		var pageResource = resourceResolver.getResource(generalConfigPagePath + LOOKUP_PATH);
		var rootParsysNode = pageResource != null ? pageResource.adaptTo(javax.jcr.Node) : null;
		var contentNode = null;

		contentNode = rootParsysNode != null ? rootParsysNode.getNodes() : null;
		contentNode = contentNode != null ? contentNode[contentNode.length - 1] : null;
		var lookupChildren;

		if (contentNode != null && contentNode.getName().indexOf('lookup') > -1) {
			lookupChildren = contentNode.hasNode('lookup') ? contentNode.getNode('lookup').getNodes() : null;
		}

		if (lookupChildren != null) {
            for (var childIdx = 0; childIdx < lookupChildren.length; childIdx++) {
                var currentChild = lookupChildren[childIdx];
                if (currentChild.hasProperty('value')) {
                    if (currentChild.getProperty('text').getString() == 'mobileLoginImagePath') {
                        mobileLoginImagePath = lookupChildren[childIdx].getProperty('value').getString();
                    } else if (currentChild.getProperty('text').getString() == 'linkLabel') {
                        loginLabel = lookupChildren[childIdx].getProperty('value').getString();
                    }
                }
            }
        }
        if (!mobileLoginImagePath) {
            mobileLoginImagePath = '/content/dam/att/uf/images/Home/logo-myatt.png';
        }
        if (!loginLabel) {
            loginLabel = 'Login';
        }
        externalizedUrl = assetsExternalizerService.externalize(resourceResolver, mobileLoginImagePath);
    }
    return {
        mobileLoginImagePath: externalizedUrl,
        loginActionURL: authLoginService.getLoginActionURL(),
        loginLabel: loginLabel
    };
});