
'use strict';

use(function() {
	var countdownBgGradStartColor = this.countdownBgGradStartColor ? this.countdownBgGradStartColor : '#c9c9c9';
	var countdownBggdirection = this.countdownBggdirection ? this.countdownBggdirection : '-180deg';
	var countdownBgcolor = this.countdownBgcolor ? this.countdownBgcolor : 'gray';
	var countdownBgGradEndColor = this.countdownBgGradEndColor ? this.countdownBgGradEndColor : countdownBgcolor;
	
    return {
        countdownBgGradStartColor : countdownBgGradStartColor,
        countdownBggdirection : countdownBggdirection,
		countdownBgcolor : countdownBgcolor,
		countdownBgGradEndColor : countdownBgGradEndColor
    }    
});