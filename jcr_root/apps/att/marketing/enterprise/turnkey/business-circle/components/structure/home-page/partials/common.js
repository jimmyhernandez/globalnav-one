// Server-side JavaScript for the headlibs.html and footlibs.html logic
'use strict';
var global = this;

use(function () {
    var globalConfigService = undefined;
    var uiAssetsJsCssVersionNumber = undefined;
    if (global.sling && global.Packages) {
        globalConfigService = global.sling
                .getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
        uiAssetsJsCssVersionNumber = globalConfigService
                .getUiAssetsJsCssVersionNumber();
    }
    return {
        uiAssetsJsCssVersionNumber : uiAssetsJsCssVersionNumber
    };
});