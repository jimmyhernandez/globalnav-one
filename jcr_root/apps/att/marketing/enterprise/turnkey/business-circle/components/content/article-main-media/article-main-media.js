"use strict";

use(function () {
	
	var CONST = {
		PROP_YOUTUBE_URL: "youtubeUrl"
	};	


    var youtubeUrl = wcm.currentPage.properties[CONST.PROP_YOUTUBE_URL] || null;
    var youtubeId = youtubeUrl != null ? youtube_parser(youtubeUrl) : null;
    var embedUrl = youtubeUrl != null ? "https://www.youtube.com/embed/"+youtubeId+"?rel=0&showinfo=0" : null;

	function youtube_parser(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

	return {
		youtubeUrl:	youtubeUrl,
        youtubeId:youtubeId,
        embedUrl:embedUrl
	};
})