// Returns the Global OSGI config values for DTM library Path

"use strict";

var global = this;

use(function () {
	var resourceResolver = resource.getResourceResolver();
    var envConfigService = undefined;
    var defaultImgAltText = '';

    if (global.sling && global.Packages) {
         globalConfigService = global.sling.
         				getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
         defaultImgAltText =  globalConfigService.getDefaultImgAltText(); 

    }

    if (global.sling && global.Packages) {
         envConfigService = global.sling.
         				getService(global.Packages.com.att.global.all.cms.common.core.services.config.EnvConfig);

    }
    
    return {
        emailSignUpUrl: envConfigService.getEmailSignUpUrl()

    };
});