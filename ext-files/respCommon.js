$(document).ready(function () {
    /**
 * goto component
 * @namespace att.entbus.goto 
 * @type {object}
 * @required jQuery library
 */
    var att = att || {};
    att.entbus = att.entbus || {};

    att.entbus.goto = (function ($) {

        var selector = {
            gotoComp: '#header .goto-container',
            gotoOptions: '#header .goto-options',
            globalMenuItems: '#leftSegMenu li a.segMenuItem',
            globalMenuPopContainer: '.gm-pop-container',
            primaryWrapper: '#primary-wrapper',
            socialWrapper: '#social-wrapper'
        };

        $(function () {
            att.entbus.goto.bindEvents();
        });

        return {

            bindEvents: function () {
                /** goto component handler */
                $(selector.gotoComp).bind('mouseover mouseout', function () {
                    $(selector.gotoOptions).toggle();
                    $(this).toggleClass('active');
                });
                /** global menu popup handler */
                $(selector.globalMenuItems).bind('mouseover mouseout', function () {
                    var $this = $(this);
                    $(selector.globalMenuPopContainer).hide();
                    $(selector.globalMenuItems).removeClass('active');
                    $this.parent('li').find(selector.globalMenuPopContainer).show();
                    $this.addClass('active');
                });
                /** To close global menu popup */
                $(selector.primaryWrapper + ', ' + selector.socialWrapper).bind('mouseover', function () {
                    $(selector.globalMenuPopContainer).hide();
                    $(selector.globalMenuItems).removeClass('active');
                });

            }
        };

    })(jQuery.noConflict());
});

$(document).ready(function () {
    (function ($) {
        function showMobMenu() {
            $("#icon-menu").removeClass('menu-icon');
            $("#icon-menu").addClass('close-icon');
            $('#icon-menu').css("margin","8px 280px 0px 0px");
        }

        function hideMobMenu(){
            $("#icon-menu").removeClass('close-icon');
            $("#icon-menu").addClass('menu-icon');
            $('#icon-menu').css("margin","15px 25px 0 0");
        }

        $("#icon-menu").click(function () {
            if ($("#icon-menu").hasClass('menu-icon')) {
                if (($(".header-wrapper").hasClass("sticky")) && (!$("#side-wrapper-header").hasClass("sticky"))) {
                    $("#side-wrapper-header").addClass("sticky");
                }
                $("#side_wrapper").show(showMobMenu());
                ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'SubNav', 'contentFriendlyName': 'Primary Navigation', 'linkName': 'Menu Icon', 'linkDestinationUrl': 'Open Mobile Nav' });
            } else {
                $("#side_wrapper").hide(hideMobMenu());
                ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'SubNav', 'contentFriendlyName': 'Primary Navigation', 'linkName': 'Close Menu Icon', 'linkDestinationUrl': 'Close Mobile Nav' });
            }
        });
    })(jQuery.noConflict());
});


$(document).ready(function () {
    (function ($) {
        $(".arrow_prod_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.product > li").addClass("active_sub_menu_items");
            $(".product").show();
            $(".arrow_prod_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_prod_m").css("display", "none");
            $(".sub_back_arrow").css("display", "none");
        });
        $(".arrow_goto_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.goto > li").addClass("active_sub_menu_items");
            $(".goto").css("display", "block");
            $(".arrow_goto_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_goto_m").css("display", "none");
        });
        $(".arrow_personal_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.personal > li").addClass("active_sub_menu_items");
            $(".personal").css("display", "block");
            $(".arrow_personal_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_personal_m").css("display", "none");
        });
        $(".arrow_business_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.business > li").addClass("active_sub_menu_items");
            $(".business").css("display", "block");
            $(".arrow_business_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_business_m").css("display", "none");
        });
        $(".arrow_about_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.about > li").addClass("active_sub_menu_items");
            $(".about").css("display", "block");
            $(".arrow_about_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_about_m").css("display", "none");
        });
        $(".arrow_account_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.account > li").addClass("active_sub_menu_items");
            $(".account").css("display", "block");
            $(".arrow_account_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_account_m").css("display", "none");
        });
        $(".arrow_blog_m").click(function () {
            $(this).parent("li").addClass("activemenu");
            $("ul.blog > li").addClass("active_sub_menu_items");
            $(".blog").css("display", "block");
            $(".arrow_blog_m").parent("li").siblings().css("display", "none");
            $(".back_arrow").css("display", "block");
            $(".arrow_blog_m").css("display", "none");
        });
        $(".back_arrow").click(function () {
            $(this).parent("li").removeClass("activemenu");
            $(".active_sub_menu_items").removeClass("active_sub_menu_items");
            $(".arrow_prod_m,.arrow_account_m,.arrow_blog_m,.arrow_goto_m,.arrow_personal_m,.arrow_business_m,.arrow_about_m").css("display", "block");
            $(".arrow_prod_m,.arrow_account_m,.arrow_blog_m,.arrow_goto_m,.arrow_personal_m,.arrow_business_m,.arrow_about_m").parent("li").siblings().css("display", "block");
            $(".sub_menu_wrap,.back_arrow").css("display", "none");
        });

        $(".sub_arrow_m").click(function () {
            $(".activemenu,.sub_arrow_m").css("display", "none");
            $(".active_sub_menu_items").removeClass("active_sub_menu_items");
            $(this).parent("li").addClass("active_sub_menu");
            $(this).parent("li").siblings().hide();
            $("li.active_sub_menu > ul").addClass("active_sub_menu_wrapper");
            $("ul.active_sub_menu_wrapper > li").addClass("active_sub_menu_items");
            $(".sub_back_arrow").css("display", "block");
        });

        $(".sub_back_arrow").click(function () {
            $(".activemenu,.sub_arrow_m").css("display", "block");
            $(".active_sub_menu_items").removeClass("active_sub_menu_items");
            $(".active_sub_menu_wrapper").removeClass("active_sub_menu_wrapper");
            $(this).parent("li").removeClass("active_sub_menu").addClass("active_sub_menu_items");
            $(this).parent("li").siblings().show().addClass("active_sub_menu_items");
            $(".sub_back_arrow").css("display", "none");
        });
    })(jQuery.noConflict());
});

$(document).ready(function () {
    (function ($) {

        function hideMobMenu(){
            $("#icon-menu").removeClass('close-icon');
            $("#icon-menu").addClass('menu-icon');
            $('#icon-menu').css("margin","15px 25px 0 0");
            $("#side_wrapper").css("background-color","transparent");
            $("#side_wrapper").hide();

            var wi = $(window).width();
            $("#wrapper").css({ "moz-box-shadow": "0 0 0", "webkit-box-shadow": "0 0 0", "box-shadow": "0 0 0", "float": "none" })
            $("#ie #wrapper").css({ "border-right": "none" });
            $("#header .logo").css("display", "block");
            $("#header .segment").css("display", "block");
            if ($("#icon-menu").hasClass('close-icon')) {
                $("#icon-menu").removeClass('close-icon');
                $("#icon-menu").addClass('menu-icon');
            }
            $('#wrapper').css({ "margin": "0px auto" });
            $('#primary-wrapper').css({ "max-width": "100%" });
            if ((wi|| document.documentElement.clientWidth) <= 767) {
                //$("#header").css("max-width","480px");
            } else if ((wi || document.documentElement.clientWidth) <= 1023) {
                //$("#header.resp-header").css("max-width","768px");
            } else {
                $("#header.resp-header").css("max-width", "980px");
                $("#header.fluid-header").css("max-width", "1280px");
            }
            //$("#header").css("float","none")
            //$("#contact-button").show();
            //if ((window.innerWidth || document.documentElement.clientWidth) > 767) {
            //$("#share-button").show();
            //}
            $(".widget-button-holder").show();
        }

        $(window).resize(function () {
            var wi = $(window).width();
            if ((wi || document.documentElement.clientWidth) >= 1024) {
                hideMobMenu();
                //hideMenu();
            } else {
                if ($("#icon-menu").hasClass('menu-icon')) {
                    if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                    } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                    } else {
                        $("#header.resp-header").css("max-width", "980px");
                        $("#header.fluid-header").css("max-width", "1280px");
                        $("#header").css("float", "inherit");

                    }
                } else if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                }
            }
            if ((window.innerWidth || document.documentElement.clientWidth) <= 479) {
                $("#formFrame .bottom-form").css("height", "650px");
            } else if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                $("#formFrame .bottom-form").css("height", "425px");
            } else {
                $("#formFrame .bottom-form").css("height", "375px");
            }
            var resizeTimer = setTimeout(function () {
                padPercent = ($("#child-copy2-overlay").height() / ($("#child-copy2-overlay").width())) * 100;
                $(".child-copy-overlay .image-block").css("padding-top", padPercent + '%');
            }, 200);
        });

        

        function hideMenu() {
            $("#side_wrapper").hide(function () {
                $("#wrapper").css({ "moz-box-shadow": "0 0 0", "webkit-box-shadow": "0 0 0", "box-shadow": "0 0 0", "float": "none" })
                $("#ie #wrapper").css({ "border-right": "none" });
                $("#header .logo").css("display", "block");
                $("#header .segment").css("display", "block");
                if ($("#icon-menu").hasClass('close-icon')) {
                    $("#icon-menu").removeClass('close-icon');
                    $("#icon-menu").addClass('menu-icon');
                }
                $('#wrapper').css({ "margin": "0px auto" });
                $('#primary-wrapper').css({ "max-width": "100%" });
                if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                    //$("#header").css("max-width","480px");
                } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                    //$("#header.resp-header").css("max-width","768px");
                } else {
                    $("#header.resp-header").css("max-width", "980px");
                    $("#header.fluid-header").css("max-width", "1280px");
                }
                //$("#header").css("float","none")
                //$("#contact-button").show();
                //if ((window.innerWidth || document.documentElement.clientWidth) > 767) {
                //$("#share-button").show();
                //}
                $(".widget-button-holder").show();
            });
        }

    })(jQuery.noConflict());
});


//Dropdown menu Global Navigation Topic

$(document).ready(function() {
    (function ($) {
    $('.dropdown').hover(
        function(){
            $("#topicsInfo").slideDown(200);
        }
    );
    $('#topicsInfo').mouseleave(
        function(){
            $("#topicsInfo").hide();
        }
    );

    })(jQuery.noConflict());
}); // end ready