package apps.att.marketing.enterprise.turnkey.business_circle.components.content.author_small_bio;

//import org.apache.felix.scr.annotations.Reference;

import java.util.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import com.adobe.cq.sightly.WCMUse;
import com.day.cq.wcm.api.Page;
import com.day.cq.search.*;
import com.day.cq.search.result.*;
import com.day.cq.search.facets.*;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

import org.apache.sling.api.resource.*;
import javax.jcr.Session;
import javax.jcr.Value;

//Sling Imports
import org.apache.sling.api.resource.ResourceResolverFactory ; 
import org.apache.sling.api.resource.ResourceResolver; 
import org.apache.sling.api.resource.Resource; 


import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.adobe.granite.security.user.UserPropertiesService;

public class AuthorSmallBio extends WCMUse {

	Boolean isAEMUser = false;
	String userID;
	String givenName;
	String familyName;
	String email;
	String aboutMe;
	String userTitle;
    String profilePage;
    String externalWebsite;
	String image;
	String defaultUserImagePath = "/libs/granite/security/clientlib/themes/default/resources/sample-user-thumbnail.100.png";
	
    @Override
    public void activate() throws Exception {
        String author = (getCurrentPage().getProperties().get("author") != null ? getCurrentPage().getProperties().get("author").toString() : null);
		String createdBy = (getCurrentPage().getProperties().get("jcr:createdBy") != null ? getCurrentPage().getProperties().get("jcr:createdBy").toString() : null);

        userID = (author != null  ? author : createdBy);
		Session session = getResourceResolver().adaptTo(Session.class);
		UserManager userManager = getResourceResolver().adaptTo(UserManager.class);
		UserPropertiesService ups = getSlingScriptHelper().getService(UserPropertiesService.class);
		UserPropertiesManager upm = ups.createUserPropertiesManager(session, getResourceResolver());
	
		Authorizable auth = (userManager.getAuthorizable(userID) != null ? userManager.getAuthorizable(userID) : userManager.getAuthorizableByPath(userID));


		if(auth != null){
			UserProperties up = upm.getUserProperties(auth, "profile");
			
			isAEMUser = true;
			givenName = ((auth.getProperty("./profile/givenName") != null && auth.getProperty("./profile/givenName").length > 0) ? auth.getProperty("./profile/givenName")[0].getString() : "");
			familyName = ((auth.getProperty("./profile/familyName") != null && auth.getProperty("./profile/familyName").length > 0) ? auth.getProperty("./profile/familyName")[0].getString() : "");
			email = ((auth.getProperty("./profile/email") != null && auth.getProperty("./profile/email").length > 0) ? auth.getProperty("./profile/email")[0].getString() : "");
			aboutMe = ((auth.getProperty("./profile/aboutMe") != null && auth.getProperty("./profile/aboutMe").length > 0) ? auth.getProperty("./profile/aboutMe")[0].getString() : "");
			userTitle = ((auth.getProperty("./profile/title") != null && auth.getProperty("./profile/title").length > 0) ? auth.getProperty("./profile/title")[0].getString() : "");
            profilePage = ((auth.getProperty("./profile/profilePage") != null && auth.getProperty("./profile/profilePage").length > 0) ? auth.getProperty("./profile/profilePage")[0].getString() : "");
    		externalWebsite = ((auth.getProperty("./profile/externalWebsite") != null && auth.getProperty("./profile/externalWebsite").length > 0) ? auth.getProperty("./profile/externalWebsite")[0].getString() : "");
			image = up.getResourcePath(UserProperties.PHOTOS + "/primary/image", null, defaultUserImagePath);

		}else{
			isAEMUser = false;
		}
    }
	public Boolean isAEMUser(){
		return isAEMUser;
	}
    public String getUserID(){
		return userID;
	}
	public String getFamilyName(){
		return familyName;
	}
	public String getGivenName(){
		return givenName;
	}
	public String getEmail(){
		return email;
	}
	public String getAboutMe(){
		return aboutMe;
	}
	public String getUserTitle(){
		return userTitle;
	}
    public String getProfilePage(){
		return profilePage;
	}
    public String getExternalWebsite(){
		return externalWebsite;
	}
	public String getImage(){
		return image;
	}
}