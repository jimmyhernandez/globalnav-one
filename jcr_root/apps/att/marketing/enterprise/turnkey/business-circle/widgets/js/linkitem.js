LinkItemWidget = CQ.Ext.extend(CQ.form.CompositeField, {
    text: "default text",
    delimiter: "|",
    hideLabel: true,
    constructor: function(config) {
        if (config.text != null) {
            this.text = config.text;
        }
        
        var defaults = {
            height: "auto",
            border: false,
            style: "padding: 0; margin-bottom: 0;",
            layoutConfig: {
                labelSeparator: CQ.themes.Dialog.LABEL_SEPARATOR
            },
            defaults: {
                msgTarget: CQ.themes.Dialog.MSG_TARGET
            }
        };
        
        CQ.Util.applyDefaults(config, defaults);
        
        LinkItemWidget.superclass.constructor.call(this, config);
        
        this.hiddenField = new CQ.Ext.form.Hidden({
            name: this.name
        });

        this.linkText = new CQ.Ext.form.TextField({
            fieldLabel: "Title",
            anchor: "100%",
            listeners: {
                change: {
                    scope:this,
                    fn: this.updateHidden
                }
            }
        });

        this.linkPath = new CQ.form.PathField({
            fieldLabel: "Path",
            anchor: "100%",
            listeners: {
                change: {
                    scope:this,
                    fn: this.updateHidden
                },
                dialogselect: {
                    scope:this,
                    fn: this.updateHiddenForPath
                }
            }
        });
        
        this.linkTarget = new CQ.Ext.form.Checkbox({
            fieldLabel: "Open in new tab?",
            width:"100%",
            anchor: "100%",
            listeners: {
                check: {
                    scope:this,
                    fn: this.updateHidden
                }
            }
        });


        this.add(this.hiddenField);
        this.add(this.linkText);
        this.add(this.linkPath);
        this.add(this.linkTarget);
    },
    processRecord: function(record, path) {
        var nodeName = this.getName();
        this.setValue(record.get(nodeName));
    },
    setValue: function(v) {
        var splitData = v.split(this.delimiter);
        
        if (splitData.length >= 2) {
            this.linkText.setValue(splitData[0]);
            this.linkPath.setValue(splitData[1]);
            
            if (splitData.length >= 3) {
                this.linkTarget.setValue(splitData[2]);
            }

            this.hiddenField.setValue(v);
        }
        return this;
    },
    updateHidden: function() {
        this.hiddenField.setValue(this.getValue());
    },
    updateHiddenForPath: function() {
        this.linkPath.setValue(this.linkPath.getValue() + ".html");
        this.updateHidden();
    },
    // overriding CQ.form.CompositeField#getValue
    getValue: function() {
        return this.getRawValue();
    },
    // overriding CQ.form.CompositeField#getRawValue
    getRawValue: function() {
        return this.linkText.getValue() + this.delimiter + this.linkPath.getValue() + this.delimiter + this.linkTarget.getValue();
    }
});

CQ.Ext.reg("linkitem", LinkItemWidget);