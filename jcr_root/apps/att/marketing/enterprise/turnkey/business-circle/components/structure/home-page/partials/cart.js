// Returns the Global OSGI config values for Cart

"use strict";

var global = this;

use(function () {

    var gobalConfigService = undefined;

    if (global.sling && global.Packages) {
         gobalConfigService = global.sling.
         				getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
    }

    return {

        wirelessCartServiceURL: gobalConfigService.getWirelessCartServiceURL(),
        mobileCartSummaryURL: gobalConfigService.getWirelessMobileCartSummaryURL()

    };
});