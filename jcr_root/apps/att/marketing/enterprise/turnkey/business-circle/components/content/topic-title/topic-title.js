/**
 * Topic Title component JS backing script
 */

"use strict";
var global = this;
use(function () {
    var tagParam = (request.getRequestParameter("tag") != null && !request.getRequestParameter("tag").getString().isEmpty() ? request.getRequestParameter("tag").getString() : null);
	var resourceResolver = resource.getResourceResolver();
	var tagManager = resourceResolver.adaptTo(global.Packages.com.day.cq.tagging.TagManager);
    var myTag = tagManager.resolve(tagParam);

    /*if(myTag==null){
		myTag = tagManager.resolve(defaultTag);
    }*/

    return {
        tagName: myTag!=null ? myTag.title : "No Tag",
        tagDescription: myTag!=null ? myTag.description : ""
    };
});
