/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

// Server-side JavaScript for the head.html logic
var global = this;

use(function () {
	Packages.com.att.global.all.cms.common.core.utils.CommonUtil.setPublishServerHeader(response);
    var WCMUtils = Packages.com.day.cq.wcm.commons.WCMUtils;
    TEMPLATE_PROP = "jcr:title";
    var resourceResolver = resource.getResourceResolver();
    pageUrl =currentPage.getPath();
    var title = currentPage.getProperties().get(TEMPLATE_PROP, "");
    if (pageUrl != null && pageUrl.indexOf("/index") != -1 ) {
     var title = currentPage.getProperties().get(TEMPLATE_PROP, "");
    }
    else {
     var childPages = currentPage.listChildren(new Packages.com.day.cq.wcm.api.PageFilter());
     while (childPages.hasNext()) {
        var page = childPages.next();
        if (page.getName() == "index")
        {
 		 title = page.getProperties().get(TEMPLATE_PROP, "");
        }
      }
    }    
    var gobalConfigService = undefined;
    var envConfigService = undefined;
    var dnsUrls = [];
    if (global.sling && global.Packages) {
    	 envConfigService = global.sling.getService(global.Packages.com.att.global.all.cms.common.core.services.config.EnvConfig);    	
         gobalConfigService = global.sling.getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
         dnsUrls = gobalConfigService.getDnsPrefetchUrls();
    }
    
   return {
        keywords: WCMUtils.getKeywords(currentPage, false),
        favIcon: resourceResolver.getResource(currentDesign.getPath() + "/favicon.ico"),
        title:title,
        dnsUrls: dnsUrls,
        goldeneyeUrl:envConfigService.getEcmsGoldeneyeURL()
    };
});