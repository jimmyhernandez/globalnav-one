<%@include file="/libs/foundation/global.jsp" %>
<%@page import="com.att.global.all.cms.common.core.utils.HashGenHelper"%><%

    String mdHash = HashGenHelper.getMd5ForResource(currentPage.getPath());

    String[] links = properties.get("links", new String[0]);
    String title = properties.get("listTitle", "");
    String displayTitle = properties.get("displayTitle", "false");
    String componentFriendlyName = properties.get("componentFriendlyName", "");
    String contentProductGroup = properties.get("contentProductGroup", "");
    pageContext.setAttribute("links", links);
    pageContext.setAttribute("listTitle", title);
    pageContext.setAttribute("displayTitle", displayTitle);
    pageContext.setAttribute("mdHash", mdHash);
    pageContext.setAttribute("componentFriendlyName", componentFriendlyName);
    pageContext.setAttribute("contentProductGroup", contentProductGroup);
    if(displayTitle.equals("false")) {
        pageContext.setAttribute("noTitleClass", "noTitle");
    } else {
        pageContext.setAttribute("noTitleClass", "");
    }
%>

<cq:includeClientLib categories="cq.widgets" />
<cq:includeClientLib categories="wcm.foundation.components.list" />
<c:choose>
    <c:when test="${empty links && wcmmode eq 'EDIT'}">
        <div class="linksList">
            <img src="/libs/cq/ui/resources/0.gif" class="cq-list-placeholder" alt="">
        </div>
    </c:when>
    <c:when test="${not empty links}">
        <div class="cq-dd-pages bizhub-list relatedProductMain">
            <div class="relatedProduct container ${noTitleClass}">
                <c:if test="${displayTitle=='true'}">
                    <h3 class="heading-sub-section">${listTitle}</h3>
                </c:if>
                <ul class="foundation-ordered-list-container">
                    <c:forEach var="l" items="${links}">
                        <c:if test="${not empty l}">
                        <%
                            String linkObj = (String)pageContext.getAttribute("l");

                                String[] linkData = linkObj.split("\\|");
                                pageContext.setAttribute("link", linkData);

                                String[] linkAttrs = (String[])pageContext.getAttribute("link");
                                String linkPath = linkAttrs[1];
                                String potentialTitle = linkAttrs[0];
                                if(!linkPath.equals("")) {
                                    String path = linkPath.replaceAll(".html", "");
                                    Resource res = resourceResolver.getResource(path);
                                    if(res!=null) {
                                        Page myPage = res.adaptTo(Page.class);
                                        String pageTitle = myPage.getTitle();
                                        if(potentialTitle.equals("")) {
                                            potentialTitle = pageTitle;
                                        }
                            %>
                            <c:set var="linkText" value="<%=potentialTitle%>"/>
                            <c:set var="linkHref" value="<%=linkPath%>"/>
                            <c:set var="linkSet" value="true"/>
                            <%
                                    } else {
                                        // assume we're an external
                                        if(potentialTitle.equals("")){
                                            potentialTitle="Set Title!";
                                        }
                            %>
                            <c:set var="linkText" value="<%=potentialTitle%>"/>
                            <c:set var="linkHref" value="<%=linkPath%>"/>
                            <%
                                    }
                                }

                        %>

                        <c:if test="${link[2] == 'true'}">
                            <c:set var="target" value="_blank"/>
                        </c:if>

                        <li class="foundation-list-item">
                            <c:choose>
                                <c:when test="${not empty linkHref}">

                                    <a href="${linkHref}" target="${target}" class="p-small" data-analytics-action="linkClick" data-analytics-code="Link_Click" data-analytics-info="{'events.contentId':'${mdHash}','events.slotFriendlyName':'${componentFriendlyName}', 'events.contentProductGroup':'${contentProductGroup}','events.linkName':'${linkText}', 'events.linkPosition':'Related Product', 'events.linkDestinationUrl':'${linkHref}'}">
                                        <div class="bizhub-link-container">
                                            <div class="bizhub-link">${linkText}</div>
                                            <div class="bizhub-link-icon" ><i class="listmarker icon-right"></i></div>
                                        </div>
                                    </a>

                                </c:when>
                                <c:otherwise>
                                    ${linkText}
                                </c:otherwise>
                            </c:choose>
                        </li>
                        </c:if>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:when>
</c:choose>
<script>
    if(window.skipAnalytics !== true) {
        $att(function(){
            var additionaldata = {};
            additionaldata = {
                'events.contentId': '${mdHash}',
                'events.slotFriendlyName': '${componentFriendlyName}',
                'events.contentSystem': 'CQ',
                'events.contentProductGroup': '${contentProductGroup}',
            };
            AttApp.analytics.sendAnalyticsEvent('impression', 'impression', additionaldata);
        });
    }
</script>