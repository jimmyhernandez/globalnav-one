"use strict";

/**
 * check if the tdata attributes needs to be displayed on the page.
 *
 */

use(function () {

    var Constants = {
        TEMPLATE_PROP: "cq:template", 
        TEMPLATE_NAME: "/apps/att/global/all/cms/common/templates/plain-page"
    };
    var displayTdataAtt = request.getParameter("tdata");
    var template = currentPage.getProperties().get(Constants.TEMPLATE_PROP, "");
    if(template != null && template.equals(Constants.TEMPLATE_NAME) && displayTdataAtt=='true') {
		displayTdataAtt =  true;
    }
    else{
		displayTdataAtt =  false;
    }

     return {

         showAttr : displayTdataAtt,

         template : template
    };

    
});