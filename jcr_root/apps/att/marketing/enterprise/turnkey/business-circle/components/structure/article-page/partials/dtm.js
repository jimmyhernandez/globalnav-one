// Returns the Global OSGI config values for DTM library Path

"use strict";

var global = this;

use(function () {

    var envConfigService = undefined;

    if (global.sling && global.Packages) {
         envConfigService = global.sling.
         				getService(global.Packages.com.att.global.all.cms.common.core.services.config.EnvConfig);
          globalConfigService = global.sling.
         				getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);         
    }

    return {

        dtmLibraryPath: envConfigService.getDtmLibraryPath(),
		dotComPrefetchURL: envConfigService.getDotComPrefetchUrl(),
        dtmDataManagerPath: envConfigService.getDtmDataManagerPath(),
        dtmDefinitionPath: envConfigService.getDtmDataDefinitionPath(),
        dtmDataMappingPath: envConfigService.getDtmDataMappingPath(),
		dtmContainerHeaderJsPath: globalConfigService.getDtmContainerHeaderJsPath(),
        dtmContainerFooterJsPath: globalConfigService.getDtmContainerFooterJsPath()

    };  
});


