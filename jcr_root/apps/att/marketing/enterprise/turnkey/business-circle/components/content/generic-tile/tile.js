/**
 * Tile column layout component Use-api script.
 * Returns the number of columns configured.
 */

"use strict"; 
var global = this;

use(function () {  

    var resourceResolver = resource.getResourceResolver();
    var selectors = request.getRequestPathInfo().getSelectors();
    var selectorsCount = selectors.length;
    var tileColumnType="";
    var tDataSlot="";
    var finalTileType='1';
    var defaultImgAltText = '';

    if (global.sling && global.Packages) {
         globalConfigService = global.sling.
                  getService(global.Packages.com.att.global.all.cms.common.core.services.config.GlobalConfig);
         defaultImgAltText =  globalConfigService.getDefaultImgAltText();         
         assetsExternalizerService = global.sling.
                  getService(global.Packages.com.att.global.all.cms.common.core.services.AssetsExternalizer);
    }

    if (selectorsCount != null && selectorsCount != 0) {
       if (selectorsCount >1)
        {
            finalTileType=selectors[0];
            tDataSlot = selectors[1];

        }
        else {
            finalTileType= selectors;
        }
    }
    var iconImagePath;
	var videoReference ;
	var videoFlag = false;
	var imageVideoType;
    var tileImagePath;
    var mobileImagePath;
	var tileImageDamPath;
    if(currentNode.hasNode("tileImage") && currentNode.getNode("tileImage").hasProperty("fileReference")) {                    
        tileImagePath = resourceResolver.map(currentNode.getNode("tileImage").getProperty("fileReference").getString()); 
		tileImageDamPath = tileImagePath;
    	//tileImagePath = assetsExternalizerService.externalize(resourceResolver, tileImagePath);
    }
    if(currentNode.hasNode("iconImage") && currentNode.getNode("iconImage").hasProperty("fileReference")) {                   
         iconImagePath = resourceResolver.map(currentNode.getNode("iconImage").getProperty("fileReference").getString()); 
         //tileImagePath = assetsExternalizerService.externalize(resourceResolver, iconImagePath);
    }

    if(currentNode.hasNode("mobileImage") && currentNode.getNode("mobileImage").hasProperty("fileReference")) {                   
         mobileImagePath = resourceResolver.map(currentNode.getNode("mobileImage").getProperty("fileReference").getString()); 
        // mobileImagePath = assetsExternalizerService.externalize(resourceResolver, mobileImagePath);

    }
	
	if(currentNode.hasProperty("imageVideo") && currentNode.hasProperty("videoRefs")) {  

        imageVideoType = resourceResolver.map(currentNode.getProperty("imageVideo").getString()); 
        if(imageVideoType == 'video'){
       	 videoReference = resourceResolver.map(currentNode.getProperty("videoRefs").getString()); 
         videoFlag = true;
        }
    }
     
    return {

         tileImage : tileImagePath,
         iconImage : iconImagePath,
         mobileImage : mobileImagePath,
		 videoRef : videoReference,
         tDataSlotId : tDataSlot,
         tileType : finalTileType,
         selectors : selectors,
         selectorsCount :selectorsCount,
         defaultImgAltText : defaultImgAltText,
		 tileImageDamPath: tileImageDamPath

    }; 


});